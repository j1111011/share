# 分享

#### 项目介绍
Genymotion-ARM-Translation.zip



#### 使用说明 [千尺浪儿-简书](https://www.jianshu.com/p/97b8250f359e)

安卓版本	代号	Genymotion-ARM-Translation	GApps
8.0 – 8.1	Oreo奥利奥	ARM_Translation_Oreo.zip	
7.0 – 7.1.2	Nougat牛轧糖		
6.0 – 6.0.1	Marshmallow棉花糖	ARM_Translation_Marshmallow.zip	
5.0 – 5.1.1	Lollipop棒棒糖	ARM_Translation_Lollipop_20160402.zip	
5.0 – 5.1.1	Lollipop棒棒糖	ARM_Translation_Lollipop.zip	
4.4 – 4.4.4	KitKat奇巧巧克力	ARM-4.4-libhoudini.zip	
4.1 – 4.3.1	Jelly Bean果冻豆	Genymotion-ARM-Translation_v1.1.zip	




1. 
- 在Genymotion中创建5.0或5.1模拟器
- 将文件“ARM_Translation_Lollipop_20160402.zip”拖到包中
- 重新启动模拟器并享受！
2. 
- 创建5.0或5.1模拟器在Genymotion 
- 拖动文件“ARM_Translation_Lollipop.zip”进入模拟器的安装包  
- 先不要重启模拟器，打开CMD命令行，输入adb shell /system/etc/houdini_patcher.sh  
修补一个ramdisk的运行框架。
- 重新启动模拟器并享受！
3. 
- 在Genymotion中创建Android 6.0模拟器。  
- 启动后，拖动ARM_Translation_Marshmallow.zip   
- 完成后重新打开模拟器。